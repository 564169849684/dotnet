﻿using System;
using FirstApp.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using FirstApp.Services;

namespace FirstApp.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class NotesController : ControllerBase
    {
        //private static List<Notes> _notes = new List<Notes> { new Notes { Id = new Guid("00000000-0000-0000-0000-000000000001"), CategoryId = "1", OwnerId = new Guid("00000000-0000-0000-0000-000000000001"), Title = "First Note", Description = "First Note Description" },
        //    new Notes { Id = Guid.NewGuid(), CategoryId = "1", OwnerId = new Guid("00000000-0000-0000-0000-000000000001"), Title = "Second Note", Description = "Second Note Description" },
        //    new Notes { Id = Guid.NewGuid(), CategoryId = "1", OwnerId = new Guid("00000000-0000-0000-0000-000000000001"), Title = "Third Note", Description = "Third Note Description" },
        //    new Notes { Id = Guid.NewGuid(), CategoryId = "1", OwnerId = new Guid("00000000-0000-0000-0000-000000000001"), Title = "Fourth Note", Description = "Fourth Note Description" },
        //    new Notes { Id = Guid.NewGuid(), CategoryId = "1", OwnerId = new Guid("00000000-0000-0000-0000-000000000001"), Title = "Fifth Note", Description = "Fifth Note Description" }
        //};

        private INoteCollectionService _noteCollectionService;


        public NotesController(INoteCollectionService noteCollectionService)
        {
            _noteCollectionService = noteCollectionService;
        }

        /// <summary>
        /// Returns a list of notes.
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        public IActionResult GetByNoteId(Guid idNote)
        {
            if (idNote == null)
            {
                return BadRequest("Note can not pe null");
            }

            var note = _noteCollectionService.Get(idNote);

            if (note == null)
            {
                return NotFound();
            }

            return Ok(note);
        }

        /// <summary>
        /// Returns a list of notes.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetNotes()
        {
            List<Notes> notes = await _noteCollectionService.GetAll();
            return Ok(notes);
        }


        [HttpPost]
        public async Task<IActionResult> CreateNotes([FromBody] Notes note)
        {

            if (note == null)
            {
                return BadRequest("Note can not pe null");
            }

            if (await  _noteCollectionService.Create(note))
            {
                return CreatedAtRoute("GetNotes", new { id = note.Id.ToString() }, note);
            }

            return NoContent();
        }

        //[HttpPut("{id}")]
        //public IActionResult UpdateNote(Guid id, [FromBody] Notes noteToUpdate)
        //{
        //    if (noteToUpdate == null)
        //    {
        //        return BadRequest("Note cannot be null");
        //    }

        //    int index = _notes.FindIndex(note => note.Id == id);
        //    if (index == -1)
        //    {
        //        return NotFound();
        //    }

        //    noteToUpdate.Id = _notes[index].Id;
        //    _notes[index] = noteToUpdate;
        //    return Ok(_notes[index]);
        //}

        //[HttpDelete("{id}")]
        //public IActionResult DeleteNote(Guid id)
        //{
        //    int index = _notes.FindIndex(note => note.Id == id);
        //    if (index == -1)
        //    {
        //        return NotFound();
        //    }

        //    _notes.RemoveAt(index);
        //    return NoContent();
        //}

        //[HttpPatch("{id}")]
        //public IActionResult UpdateTitleNote(Guid id, [FromBody] string title)
        //{
        //    if (string.IsNullOrEmpty(title))
        //    {
        //        return BadRequest("The string cannot be null");
        //    }
        //    int index = _notes.FindIndex(note => note.Id == id);
        //    if (index == -1)
        //    {
        //        return NotFound();
        //    }

        //    _notes[index].Title = title;
        //    return Ok(_notes[index]);
        //}


    }
}
