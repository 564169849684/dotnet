﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirstApp
{
    public class Category
    {
        private string _name;
        private string _id;

        public Category(string name, string id)
        {
            _name = name;
            _id = id;
        }

        public string Name
        {
            get => _name;
            set => _name = value;
        }

        public string Id
        {
            get => _id;
            set => _id = value;
        }
    }
}
