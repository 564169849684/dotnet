﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirstApp.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CategoriesController : Controller
    {
        private List<Category> _categories = new List<Category>()
        {
            (new Category("To Do", "")),
            (new Category("Doing", "")),
            (new Category("Done", ""))
        };

        public CategoriesController()
        {

        }

        /// <summary>
        /// Get request for retrieving all documents - categories.
        /// </summary>
        /// <returns></returns>
        [HttpGet("")]
        public IActionResult GetCategories()
        {
            return Ok(_categories);
        }

        /// <summary>
        /// Post request for adding a new document - category.
        /// </summary>
        /// <param name="bodyCategory"></param>
        /// <returns></returns>
        [HttpPost("")]
        public IActionResult PostCategory([FromBody] Category bodyCategory)
        {
            return Ok(bodyCategory);
        }
    }
}
