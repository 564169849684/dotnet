﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace FirstApp.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class NotesController: ControllerBase
    {
        public NotesController()
        {
            
        }

        /// <summary>
        /// Returns a list of notes.
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        public IActionResult GetWithParameters(string id, string id2, string id3)
        {
            return Ok($"id: {id} | id2: {id2} | id3: {id3}");
        }

        /// <summary>
        /// Returns a list of notes.
        /// </summary>
        /// <returns></returns>
        [HttpGet("")]
        public IActionResult Get()
        {
            return Ok();
        }

        /// <summary>
        /// Post a list of notes.
        /// </summary>
        /// <returns></returns>
        [HttpPost("")]
        public IActionResult Post([FromBody] Notes bodyContet)
        {
            return Ok(bodyContet);
        }
    }
}
